\documentclass{beamer}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{graphicx}
\usepackage[autoplay]{animate}
\usetheme{Madrid}
\setbeamercolor{normal text}{fg=white}
\usebackgroundtemplate{\includegraphics[width=\paperwidth,height=\paperheight]{images/bgimg.jpg}}
\title{Project Shmup Game}
\author{Ashita, Rivva, Radha}
\institute{Shmupters}
\date{\today}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\section{Objective}
\begin{frame}
\frametitle{Objective}
Our aim is to explore the intersection of game development and programming by creating a shmup game using C\# and Unity, fostering hands-on learning, and developing problem-solving skills. We delved into game design principles, C\# scripting, and Unity's features to create an engaging gaming experience.

After trying to create a Space Invaders game in Unity, we decided to further enhance our learning by recreating the game using Pygame, a popular game development library in Python. This exercise helped us in understanding different game development frameworks and reinforced the core concepts of game programming and OOP.
\end{frame}

\section{Pygame vs. Unity for 2D Space Invader}
\begin{frame}
\frametitle{Pygame vs. Unity for 2D Space Invader}
\begin{itemize}
    \item \textbf{Purpose and Complexity:}
    \begin{itemize}
        \item Pygame is a Python library specifically designed for 2D game development. It’s lightweight and well-suited for simple games.
        \item Unity, on the other hand, is a powerful game engine that supports both 2D and 3D development. It offers advanced features, making it suitable for more complex projects.
    \end{itemize}
    \item \textbf{Graphics Capabilities:}
    \begin{itemize} 
        \item While Pygame provides solid 2D graphics capabilities, it’s not as advanced as Unity. However, it’s sufficient for creating visually appealing games.
        \item Unity excels in both 2D and 3D graphics, with sophisticated tools and a wide range of features.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Pygame vs. Unity for 2D Space Invader}
\begin{itemize}
    \item \textbf{Performance:}
    \begin{itemize}
        \item Some argue that Pygame might have performance limitations, but for a straightforward game like Space Invaders, it should be more than adequate.
        \item Switching to Unity solely due to performance concerns without evidence of actual problems isn’t necessary.
    \end{itemize}
    \item \textbf{Customization:}
    \begin{itemize}
        \item Pygame allows you to create custom sprites, animations, and effects easily.
        \item Unity offers more flexibility but comes with a steeper learning curve.
    \end{itemize}
\end{itemize}
\end{frame}

\section{Pygame}
\begin{frame}
\frametitle{Game Mechanics}
\begin{columns}
    \begin{column}{0.6\textwidth}
        \begin{itemize}
            \item \textbf{Spaceship Control}: Player controls a spaceship that can move left and right and shoot bullets.
            \item \textbf{Alien Movement}: Aliens move horizontally and change direction upon reaching the screen edges.
            \item \textbf{UFO Appearance}: A UFO appears randomly and shoots at the player.
        \end{itemize}
    \end{column}
    \begin{column}{0.4\textwidth}
        \includegraphics[width=\textwidth]{images/game.png} % Add your image here
    \end{column}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Features}
\begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item \textbf{Double Shot Power-up}: Allows the player to shoot two bullets simultaneously for a limited time.
            \item \textbf{Shield Power-up}: Provides temporary invincibility to the player's spaceship.
            \item \textbf{Score System}: Displays the player's score based on the number of aliens destroyed.
            \item \textbf{Health Bar}: Shows the spaceship's remaining health.
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \includegraphics[width=\textwidth]{images/power_up.jpg} % Add your first image here
       % Add your second image here
    \end{column}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Sound and Graphics}
\begin{itemize}
    \item \textbf{Sound Effects}: Added sounds for shooting, alien kills, and spaceship explosions.
    \item \textbf{Background and Sprites}: Used custom images for the background, spaceship, aliens, bullets, and explosions.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Implementation}
\begin{itemize}
    \item \textbf{Spaceship Class}: Handles movement, shooting, health management, and power-ups.
    \item \textbf{Alien Class}: Controls alien movement and shooting.
    \item \textbf{Bullet and Explosion Classes}: Manage bullet behavior and visual explosion effects.
    \item \textbf{PowerUp Class}: Implements power-ups for double shots and shields.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Challenges and Solutions}
\begin{itemize}
    \item \textbf{Collision Detection}: Implemented pixel-perfect collision using Pygame’s \texttt{collide\_mask} method.
    \item \textbf{Timing and Cooldowns}: Managed shooting intervals and power-up durations with time tracking.
    \item \textbf{Game Over and Win Conditions}: Created functions to display game over and victory screens.
\end{itemize}
\end{frame}

\section{Learnings}
\begin{frame}
\frametitle{Learnings}
\begin{itemize}
    \item \textbf{Understanding Game Loops and Event Handling:}
    \begin{itemize}
        \item Game Loops: We learned how the game loop is central to any game application, updating the game state, handling inputs, and rendering graphics.
        \item Event Handling: Handling user inputs through event loops was a fundamental part of our development process.
    \end{itemize}
    \item \textbf{Sprite and Group Management:}
    \begin{itemize}
        \item Sprites: Working with Pygame's Sprite class allowed us to manage game objects and helped us organize the code and manage the rendering and updating of these objects more efficiently.
        \item Groups: We utilized Pygame's Group to handle multiple sprites at once, which made it easier to update and draw collections of sprites, such as the alien group and bullet group.
    \end{itemize}
\end{itemize}
\end{frame}

\section{Learnings - 2}
\begin{frame}
\frametitle{Learnings}
\begin{itemize}
    \item \textbf{Collision Detection:}
    \begin{itemize}
        \item Collision Handling: Implementing collision detection with \texttt{pygame.sprite.spritecollide()} and \texttt{pygame.sprite.collide\_mask()} taught us how to manage interactions between different game objects.
    \end{itemize}
    \item \textbf{Game Mechanics and Balancing:}
    \begin{itemize}
        \item Game Mechanics: Implementing features like shooting, enemy spawning, power-ups, and health systems required designing core mechanics that keep the game engaging.
        \item Difficulty Balancing: We spent time adjusting the difficulty, such as altering alien speed based on player score and implementing power-ups like double-shot and shield.
    \end{itemize}
    \item \textbf{Sound and Graphics:}
    \begin{itemize}
        \item Sound Integration: Adding sound effects for shooting, invader kills, and explosions made the game more immersive.
    \end{itemize}
\end{itemize}
\end{frame}

\section{Outro}
\begin{frame}
\centering
\animategraphics[autoplay,loop,width=\linewidth]{20}{shmup-video/shmupThumbnail-}{0}{47}
\end{frame}

\end{document}

